# -*- coding: utf-8 -*-
"""
Created on Thu Dec  8 11:22:01 2022

@author: hubsa
"""

from abc import ABC, abstractmethod

import random

class Display(ABC):
    @abstractmethod
    def update(self, temperature:float, Humiditi:float, Pressure:float):
        pass
    
class WeatherData():
    def __init__(self):
        self._displays = []
        
    def temperature(self):
        return 16.6
        
    
    def Humiditi(self):
        return 30.0
    
    def Pressure(self):
        return 1080.6
            
    def add_display(self, display:Display):
        self._displays.append(display)
        pass
    
    def MesureChange(self):
        temp = self.temperature()
        hum = self.Humiditi()
        press = self.Pressure()
        for display in self._displays:
            display.update(temp, hum, press)
        pass
    
class StatsDisplay(Display):
    
    def update(self, temperature:float, Humiditi:float, Pressure:float):
        print(f'StatsDisplay:{temperature}°, {Humiditi}%, {Pressure}bar')
        pass

class CurrentDisplay(Display):
    
    def update(self, temperature:float, Humiditi:float, Pressure:float):
        print(f'StatsDisplay:{temperature}°, {Humiditi}%, {Pressure}bar')
        pass


    
if __name__ == '__main__':

    currentDisplay = CurrentDisplay()
    statsDisplay = StatsDisplay()
    weatherData = WeatherData()
    weatherData.add_display(currentDisplay)
    weatherData.MesureChange()
    weatherData.add_display(statsDisplay)
    weatherData.MesureChange()
    # test = WeatherData()
    # test.MesureChange()