# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 13:55:58 2022

@author: hubsa
"""
from abc import ABC, abstractclassmethod
class Person:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f'Person(name={self.name})'
    
class PersonStorage(ABC): 
    def save(self, person):
        pass
class PersonXML(PersonStorage):
    def save(self, person):
        
        print(f'Save the {person} to a XML file')

if __name__ == '__main__':
    Person = Person('John Doe')
    storage = PersonXML()
    storage.save(Person)
    