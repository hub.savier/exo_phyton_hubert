# -*- coding: utf-8 -*-
"""
Created on Thu Dec  8 08:30:14 2022

@author: hubsa
"""

from abc import ABC, abstractmethod



class Flyable(ABC):
    @abstractmethod
    def fly(self):
        pass

class Fly1(Flyable):
    
    def fly(self):
        print("vol vol")

class Fly2(Flyable):
    
    def fly(self):
        print("vol")

class Duck(ABC):
    def __init__(self, flyable:Flyable):
        self._flyable = flyable
        
    def fly(self):
        self._flyable.fly()
        
    
    
    def quack(self):
        pass
    
    @abstractmethod
    def Display(self):
        pass

class MallardDuck(Duck):
    
    def __init__(self, flyable: Flyable):
        super().__init__(flyable)
        
    def quack(self):
        print("quack")
    
    def Display(self):
        pass
class RedheadDuck(Duck):

    def quack(self):
        print("quack quack")
    
    def Display(self):
        pass
    
if __name__ == '__main__':
    fly = Fly1()
    fly = Fly2()
    mallardDuck = MallardDuck(fly)
    mallardDuck.fly()
    mallardDuck.setFlyable(fly2)
    mallardDuck.fly()
        
        