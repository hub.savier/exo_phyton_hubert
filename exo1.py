# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 08:37:34 2022

@author: hubsa
"""

import time
import progressbar

for i in progressbar.progressbar(range(100)):
    time.sleep(0.05)
    